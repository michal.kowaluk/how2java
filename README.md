# Omówienie środowiska pracy
- [ ] IntelliJ IDEA, Gradle, Docker
- [ ] SourceTree, GitLab, JFrog, Git-Flow

# Podstawy języka Java
- [ ] Typy danych
- [ ] Zmienne
 - [ ] Operatory
 - [ ] Wyrażenia sterujące
 - [ ] Tablice
 - [ ] Obiekty
 - [ ] Klasy
 - [ ] Deklarowanie obiektów
 - [ ] Przypisywanie zmiennych referencyjnych do obiektów
 - [ ] Metody
 - [ ] Konstruktory
 - [ ] Przeciążanie metod i konstruktorów
 - [ ] Obiekty w parametrach
 - [ ] Referencje
 - [ ] Rekurencja
 - [ ] Kontrola dostępu
 - [ ] Składowe statyczne
 - [ ] Klasy zagnieżdżone i wewnętrzne
 - [ ] Metody ze zmienną ilością argumentów
 - [ ] Dziedziczenie
 - [ ] Hierarchia wielopoziomowa
 - [ ] Przesłanianie metod
 - [ ] Przydzielanie metod
 - [ ] Klasy abstrakcyjne
 - [ ] Pakiety
 - [ ] Interfejsy
 - [ ] Obsługa wyjątków
 - [ ] Wielowątkowość
 - [ ] Typy wyliczeniowe
 - [ ] Opakowywanie typów
 - [ ] Adnotacje
 - [ ] Obsługa wejścia-wyjścia
 - [ ] Typy sparametryzowane
 - [ ] Podstawowe wzorce projektowe

# Zaawansowane zastosowania języka Java
 - [ ] Sortowanie tablic
 - [ ] Sortowanie łańcuchów znaków
- [ ]  Sortowanie tablic równoległych
- [ ]  Przeszukiwanie łańcuchów znaków
- [ ]  Scalanie list
 - [ ] Wyświetlanie określonych obiektów
- [ ]  Metody akcesorów i mutatorów
- [ ]  Tablice obiektów
- [ ]  Operacje na listach powiązanych (LinkedList)
- [ ]  Abstrakcyjne typy danych
- [ ]  Stosy
- [ ]  Kolejki
- [ ]  Zaawansowana rekurencja
- [ ]  Drzewa binarne
- [ ]  Zaawansowane metody sortowania
- [ ]  Haszowanie

# Wprowadzenie do Java Enterprise Edition
- [ ]  Konfiguracja poprzez Gradle
- [ ]  Serwer aplikacji
- [ ]  Servlet
- [ ]  REST
- [ ]  Hibernate
- [ ]  JPQL
- [ ]  WebSockets
- [ ]  Wstrzykiwanie zależności
- [ ]  Zaawansowane wzorce projektowe 
