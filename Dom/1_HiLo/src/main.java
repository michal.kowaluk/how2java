import java.util.Scanner;

public class main {
 public static void main(String[] args) {
     Scanner scan = new Scanner(System.in); //scanner
     String playAgain = "";
     int numberFrom = 0;
     int numberTo = 0;
     System.out.println("Podaj liczbę od:");
     numberFrom = scan.nextInt();
     System.out.println("Podaj liczbę do:");
     numberTo = scan.nextInt();
     do {
         int randomizedNumber = (int) (Math.random() * numberTo + numberFrom); //Random number
         int guess = 0;
         while (guess != randomizedNumber) {
             System.out.println("Podaj liczbę pomiędzy " + numberFrom + " a " + numberTo );
             guess = scan.nextInt();
             int countGuess = 0;
             System.out.println("Wybrałeś: " + guess);
             countGuess++;
             if (randomizedNumber < guess)
                 System.out.println(guess + ": Za wysoka liczba");
             else if (randomizedNumber > guess)
                 System.out.println(guess + ": Za niska liczba");
             else
                 System.out.println("Wygranko. Udało Ci się za: " + countGuess + " razem.");
         }
         System.out.println("Czy chcesz grać ponownie? (y/n)");
         playAgain = scan.next();
     } while (playAgain.equalsIgnoreCase("y"));
 }
}
